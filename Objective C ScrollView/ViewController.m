//
//  ViewController.m
//  Objective C ScrollView
//
//  Created by Bryan Winmill on 8/31/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];//Initalizing scrollview memory and allocating, and setting frame
    scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3);
    //scrollview.backgroundColor = [UIColor yellowColor];
    [scrollview setPagingEnabled:YES];
    [self.view addSubview:scrollview];
    
    UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*0, self.view.frame.size.height*0, self.view.frame.size.width, self.view.frame.size.height)];
    view1.backgroundColor = [UIColor purpleColor];
    [scrollview addSubview:view1];
    
    UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*0, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
    view2.backgroundColor = [UIColor greenColor];
    [scrollview addSubview:view2];
    
    UIView* view3 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*0, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    view3.backgroundColor = [UIColor redColor];
    [scrollview addSubview:view3];
    
    UIView* view4 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, self.view.frame.size.height*0, self.view.frame.size.width, self.view.frame.size.height)];
    view4.backgroundColor = [UIColor blueColor];
    [scrollview addSubview:view4];
    
    UIView* view5 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
    view5.backgroundColor = [UIColor grayColor];
    [scrollview addSubview:view5];
    
    UIView* view6 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    view6.backgroundColor = [UIColor blackColor];
    [scrollview addSubview:view6];

    UIView* view7 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*0, self.view.frame.size.width, self.view.frame.size.height)];
    view7.backgroundColor = [UIColor cyanColor];
    [scrollview addSubview:view7];
    
    UIView* view8 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
    view8.backgroundColor = [UIColor yellowColor];
    [scrollview addSubview:view8];
    
    UIView* view9 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    view9.backgroundColor = [UIColor brownColor];
    [scrollview addSubview:view9];



}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
