//
//  main.m
//  Objective C ScrollView
//
//  Created by Bryan Winmill on 8/31/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
